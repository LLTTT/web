<!doctype html>
<?php
$host_url = './';
$mapname = '';
if (isset($_GET['mapname'])) {
  $mapname = $_GET['mapname'];
}

$mapname_nice = '...';
$map_images = array();
$map_image = $host_url.'mario.png';


if ($mapname) {
  $mapname_nice = preg_replace("/ttt_([a-zA-Z0-9_-]+)/", "$1", $mapname);
  $mapname_nice = str_replace("_"," ",$mapname_nice);
  $mapname_nice = ucwords($mapname_nice);
  $mapname_nice = ' '.$mapname_nice;

  // get a random map image
  $I=1;
  while(file_exists('maps/'.$mapname."/".$I.".jpg")){
    $map_images[] = $host_url.'maps/'.$mapname.'/'.$I.'.jpg';
    $I++;
  };

  $map_image = $map_images[array_rand($map_images)];
}

?>
<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="style.css?">
    <style>
      html {
        <?php if ($map_image == $host_url.'mario.png') {?>background-size:50%;<?} ?>
        background-image:url('<?=$map_image?>');
      }
    </style>
  </head>
  <body>
    <div id="header"><h1>A LUELinks Extremist Muslim Terrorist Gaming Experience</h1></div>
    <div id="container">

      <div id="rules">
        <h2>The Rules</h2>
        <h4>Not a replacement for reading the rules (Press F1 in-game)</h4>
        <div class="rule">
          <h3>The Pubkill Rule</h3>
          <p>We like to keep things moving. If a certain amount of time passes and the remaining traitors have not made a move, community members may invoke The Pubkill Rule. When in effect, non-regular players on the server will be systematically slaughtered in order to progress the round. This is not considered RDM.</p>
        </div>
        
        <div class="rule">
          <h3>RDM</h3>
          <p>RDM is when an innocent begins randomly killing other innocents without reason, deathmatch-style. If you have reason to believe someone is the traitor, you are allowed to kill them. This is not RDM. If wrong, be prepared to face the consequences from your fellow terrorists. If you make up bullshit reasons or revenge kill, we will still ban the shit out of you.</p>
        </div>
        
        <div class="rule">
          <h3>Rooftop Safety</h3>
          <p>If you purposely stand in a precarious position such as a building ledge or cliff, anyone is allowed to attempt to push you off. This is not RDM, but may be considered traitorous.</p>
        </div>
        <div style="clear:both;"></div>
      </div>
    </div>
    <div id="loadingbar">
      <div>You are joining<?=$mapname_nice?></div>
      <div id="dlf">Loading...</div>
    </div>
          <script type="text/javascript">
            var Open = false;
            var DownloadingText = document.getElementById('dlf');
            
            var DLF_PRE = "Downloading ";
            var DLF_NOTHING = "nothing!";
            
            var TOTAL_FILES = 0;
            var REMAIN_FILES = 0;
            var CUR_FILE = "";
            
            function ReRender() {
                if (CUR_FILE == "" || TOTAL_FILES === 0) {
                    DownloadingText.innerHTML = "Loading...";
                    return false;
                } 
                
                var perct = " (File "+String(TOTAL_FILES - REMAIN_FILES)+" of "+TOTAL_FILES+"; "+Math.round((REMAIN_FILES/TOTAL_FILES)*100)+"%)"
                var txt = DLF_PRE + '"'+CUR_FILE+'"' + perct;
                DownloadingText.innerHTML = txt;
                return true;
            }
            
            ReRender();
            
            function DownloadingFile(filename) {
                window.CUR_FILE = filename;
                ReRender();
            }
            
            function SetStatusChange(status) {
                if (status != "Sending client info...") {
                    return true;
                }
                CUR_FILE = "";
                TOTAL_FILES = 0;
                REMAIN_FILES = 0;
                ReRender();
            }
            
            function SetFilesNeeded(needed) {
                REMAIN_FILES = needed;
                ReRender();
            }
            
            function SetFilesTotal(total) {
                TOTAL_FILES = total;
                ReRender();
            }
    </script>
  </body>
</html>